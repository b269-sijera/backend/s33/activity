fetch("https://jsonplaceholder.typicode.com/todos/")
.then((response) => response.json())
.then((json) => {
	let titles = json.map(item => item.title);
	console.log(titles);
})



// fetch('https://jsonplaceholder.typicode.com/todos')
// .then(response => response.json())
// .then(data => {
//    data.map((title) => {
//     return elem.title 
//   })
// }).then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "Get",
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos/", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: false,
		title: "Created To DO List Item",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		status: "Pending",
		title: "Updated To DO List Item",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: "07/09/21",
		status: "Complete",
		title: "delectus aut auter",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE",
});